#ifndef LEVEL_H
#define LEVEL_H

#include "sdl2.h"
#include "donut.h"
// --------------- //
// Data structures //
// --------------- //

enum wallType {
    WALL_UP,
    WALL_RIGHT,
    WALL_DOWN,
    WALL_LEFT
};

struct Level {
    SDL_Rect *walls;        // The walls of the level
    SDL_Rect *platforms;    // The platforms of the level
    struct Donut **donuts;  // The donuts
    SDL_Renderer *renderer; // The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a level.
 *
 * @param renderer  The renderer
 * @return          A pointer to the create level, NULL if there was an error.
 */
struct Level *Level_create(SDL_Renderer *renderer);

/**
 * Delete the given level.
 *
 * @param level  The level to be deleted
 */
void Level_delete(struct Level *level);

/**
 * Renders the level with a renderer.
 *
 * @param level  The level to be rendered
 */
void Level_render(struct Level *level);

bool Level_checkCollision(SDL_Rect *rect, struct Level *level);

void Level_updateDonuts(struct Level *level, SDL_Rect *rect);

bool Level_hasNoDonuts(struct Level *level);

#endif
