#include "game.h"
#include "sdl2.h"
#include "constants.h"

// --------------------------- //
// Private function prototypes //
// --------------------------- //

void Game_moveCharacter(struct Character *character, struct Level *level);

bool Game_checkCollision(struct Character *character, struct Level *level);

void Game_updateDonuts(struct Level *level, struct Character *character);

void Game_end(struct Game *game);

struct Game *Game_initialize(SDL_Renderer *renderer) {
    struct Game *game;
    game = (struct Game*)malloc(sizeof(struct Game));
    game->renderer = renderer;
    game->level = Level_create(renderer);
    game->character = Character_create(renderer);
    game->state = GAME_PLAY;
    return game;
}

void Game_delete(struct Game *game) {
    if (game != NULL) {
        Level_delete(game->level);
        Character_delete(game->character);
        free(game);
    }
}

void Game_run(struct Game *game) {
    SDL_Event e;
    while (game->state == GAME_PLAY) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                game->state = GAME_QUIT;
            } else {
                Character_handleEvent(game->character, &e);
            }
        }

        Game_moveCharacter(game->character, game->level);
        Game_updateDonuts(game->level, game->character);
        Character_updateRow(game->character);

        SDL_SetRenderDrawColor(game->renderer, 0xcf, 0xee, 0xa1, 0x00);
        SDL_RenderClear(game->renderer);
        Level_render(game->level);
        Character_render(game->character);
        SDL_RenderPresent(game->renderer);

        if (Level_hasNoDonuts(game->level)) {
            Game_end(game);
        }
    }
}

void Game_moveCharacter(struct Character *character, struct Level *level) {
    character->position->x += character->velocity->x;
    character->boxCollider.x = character->position->x;

    if(Game_checkCollision(character, level)) {
        character->position->x -= character->velocity->x;
        character->boxCollider.x = character->position->x;
    }

    int lastPositionY = character->position->y;
    character->position->y += character->velocity->y;
    character->position->y += CHARACTER_GRAVITY;
    character->boxCollider.y = character->position->y;

    if(Game_checkCollision(character, level)) {
        character->position->y -= character->velocity->y;
        character->position->y -= CHARACTER_GRAVITY;
        character->boxCollider.y = character->position->y;
    }
    character->state.touchGround = (character->velocity->y == 0)
            && ((lastPositionY - character->position->y) == 0);
}

bool Game_checkCollision(struct Character *character, struct Level *level) {
    return Level_checkCollision(&(character->boxCollider), level);
}

void Game_updateDonuts(struct Level *level, struct Character *character) {
    Level_updateDonuts(level, &(character->boxCollider));
}

void Game_end(struct Game *game) {
    struct Spritesheet *sprite = Spritesheet_create(END_GAME_FILENAME,
                                                    1, 1, 1, 1.0,
                                                    game->renderer);
    SDL_Event e;
    while (game->state != GAME_MENU && game->state != GAME_QUIT) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                game->state = GAME_QUIT;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        game->state = GAME_MENU;
                        break;
                }
            }
        }
        Spritesheet_render(sprite, END_GAME_X, END_GAME_Y, 0);
        SDL_RenderPresent(game->renderer);
    }
}
