#include "sdl2.h"
#include "constants.h"
#include "donut.h"
#include "animated_spritesheet.h"

// ---------------- //
// Public functions //
// ---------------- //

struct Donut *Donut_create(SDL_Renderer *renderer, int x, int y) {
    struct Donut *donut = (struct Donut *) malloc(sizeof(struct Donut));
    donut->posX = x;
    donut->posY = y;
    donut->eaten = false;
    donut->renderer = renderer;
    
    donut->animatedSpritesheet = 
            AnimatedSpritesheet_create(DONUT_SPRITESHEET, DONUT_NUM_ROWS,
                                       DONUT_NUM_COLS, DONUT_NUM_SPRITES,
                                       DONUT_SCALE, DONUT_BETWEEN_FRAME, renderer);

    int width = donut->animatedSpritesheet->spritesheet->spriteWidth;
    int height = donut->animatedSpritesheet->spritesheet->spriteHeight;
    donut->boxCollider = (SDL_Rect){.x = donut->posX, 
                                    .y = donut->posY,
                                    .w = (int)(DONUT_SCALE * width), 
                                    .h = (int)(DONUT_SCALE * height)};
    AnimatedSpritesheet_run(donut->animatedSpritesheet);
    return donut;
}

void Donut_delete(struct Donut *donut) {
    if (donut != NULL) {
        AnimatedSpritesheet_delete(donut->animatedSpritesheet);
        free(donut);
    }
}

void Donut_render(struct Donut *donut) {
    if (!donut->eaten) {
        AnimatedSpritesheet_render(donut->animatedSpritesheet,
                                   donut->posX,
                                   donut->posY);
    }
}