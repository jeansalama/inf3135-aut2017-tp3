#ifndef DONUT_H
#define DONUT_H

#include "sdl2.h"
#include "animated_spritesheet.h"
#include <stdbool.h>

// --------------- //
// Data structures //
// --------------- //

struct Donut {
    int posX;                                        // His x-coordinate
    int posY;                                        // His y-coordinate
    struct AnimatedSpritesheet *animatedSpritesheet; // His spritesheet
    bool eaten;                                      // True if it has been eaten
    SDL_Rect boxCollider;                            // His box collider
    SDL_Renderer *renderer;                          // The renderer
};

// --------- //
// Functions //
// --------- //

struct Donut *Donut_create(SDL_Renderer *renderer, int x, int y);

void Donut_delete(struct Donut *donut);

void Donut_render(struct Donut *donut);

#endif