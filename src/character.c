#include "constants.h"
#include "character.h"

struct Character *Character_create(SDL_Renderer *renderer) {
    struct Character *character;
    character = (struct Character*)malloc(sizeof(struct Character));
    character->renderer = renderer;

    character->position = (struct Point*)malloc(sizeof(struct Point));
    character->position->x = CHARACTER_INIT_POSITION_X;
    character->position->y = CHARACTER_INIT_POSITION_Y;

    character->velocity = (struct Point*)malloc(sizeof(struct Point));
    character->velocity->x = 0;
    character->velocity->y = 0;

    character->spriteRow = CHARACTER_IDLE;

    character->state.up = false;
    character->state.left = false;
    character->state.right = false;
    character->state.touchGround = true;

    character->animatedSpritesheet =
            AnimatedSpritesheet_create(CHARACTER_SPRITESHEET, CHARACTER_NUM_ROWS,
                                       CHARACTER_NUM_COLS, CHARACTER_NUM_SPRITES,
                                       CHARACTER_SCALE, CHARACTER_BETWEEN_FRAME, renderer);

    int width = character->animatedSpritesheet->spritesheet->spriteWidth;
    int height = character->animatedSpritesheet->spritesheet->spriteHeight;
    character->boxCollider = (SDL_Rect){.x = character->position->x,
                                        .y = character->position->y,
                                        .w = (int)(CHARACTER_SCALE * width / 2),
                                        .h = (int)(CHARACTER_SCALE * height)};
    AnimatedSpritesheet_setRow(character->animatedSpritesheet, character->spriteRow);
    AnimatedSpritesheet_run(character->animatedSpritesheet);
    return character;
}

void Character_delete(struct Character *character) {
    if (character != NULL) {
        AnimatedSpritesheet_delete(character->animatedSpritesheet);
        free(character->position);
        free(character->velocity);
        free(character);
    }
}

void Character_render(struct Character *character) {
    int width = character->animatedSpritesheet->spritesheet->spriteWidth;
    AnimatedSpritesheet_render(character->animatedSpritesheet,
                               character->position->x - width/4 + CHARACTER_OFFSET,
                               character->position->y);
}

void Character_handleEvent(struct Character *character, SDL_Event *event) {
    if (event->type == SDL_KEYDOWN && event->key.repeat == 0) {
        switch(event->key.keysym.sym) {
            case SDLK_UP:
                character->velocity->y -= CHARACTER_VELOCITY_Y;
                character->state.up = true;
                break;
            case SDLK_LEFT:
                character->velocity->x -= CHARACTER_VELOCITY_X;
                character->state.left = true;
                break;
            case SDLK_RIGHT:
                character->velocity->x += CHARACTER_VELOCITY_X;
                character->state.right = true;
                break;
        }
    } else if (event->type == SDL_KEYUP && event->key.repeat == 0) {
        switch(event->key.keysym.sym) {
            case SDLK_UP:
                character->velocity->y += CHARACTER_VELOCITY_Y;
                character->state.up = false;
                break;
            case SDLK_LEFT:
                character->velocity->x += CHARACTER_VELOCITY_X;
                character->state.left = false;
                break;
            case SDLK_RIGHT:
                character->velocity->x -= CHARACTER_VELOCITY_X;
                character->state.right = false;
                break;
        }
    }
}

void Character_updateRow(struct Character *character) {
    enum SpriteRow lastRow = character->spriteRow;

    if (character->state.up || !character->state.touchGround) {
        if (character->state.left) {
            character->spriteRow = CHARACTER_JUMPING_LEFT;
        } else if (character->state.right) {
            character->spriteRow = CHARACTER_JUMPING_RIGHT;
        } else {
            character->spriteRow = CHARACTER_JUMPING_VERTICALLY;
        }
    } else {
        if (character->state.left) {
            character->spriteRow = CHARACTER_WALKING_LEFT;
        } else if (character->state.right) {
            character->spriteRow = CHARACTER_WALKING_RIGHT;
        } else {
            character->spriteRow = CHARACTER_IDLE;
        }
    }
    if (character->spriteRow != lastRow) {
        AnimatedSpritesheet_setRow(character->animatedSpritesheet, character->spriteRow);
    }
}