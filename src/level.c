#include "level.h"
#include "sdl2.h"
#include "constants.h"
#include "donut.h"


SDL_Rect *Level_createWalls() {
    SDL_Rect *walls = (SDL_Rect*) malloc(WALLS_NUMBER * sizeof(SDL_Rect));

    walls[WALL_UP] = (SDL_Rect){.x = 0, .y = 0, .w = SCREEN_WIDTH, .h = VERTICAL_WIDTH};
    walls[WALL_DOWN] = (SDL_Rect){.x = 0, .y = SCREEN_HEIGHT - VERTICAL_WIDTH,
                                  .w = SCREEN_WIDTH, .h = VERTICAL_WIDTH};
    walls[WALL_LEFT] = (SDL_Rect){.x = 0, .y = 0, .w = HORIZONTAL_WIDTH, .h = SCREEN_HEIGHT};
    walls[WALL_RIGHT] = (SDL_Rect){.x = SCREEN_WIDTH - HORIZONTAL_WIDTH, .y = 0,
                                   .w = HORIZONTAL_WIDTH, .h = SCREEN_HEIGHT};
    
    return walls;
}

SDL_Rect *Level_createPlatforms() {
    SDL_Rect *platforms = (SDL_Rect*) malloc(PLATFORMS_NUMBER * sizeof(SDL_Rect));

    platforms[0] = (SDL_Rect){.x = 3*SCREEN_WIDTH/4, .y = 2*SCREEN_HEIGHT/3,
                              .w = SCREEN_WIDTH/4, .h = VERTICAL_WIDTH};
    platforms[1] = (SDL_Rect){.x = 0, .y = SCREEN_HEIGHT/3,
                              .w = SCREEN_WIDTH/3, .h = VERTICAL_WIDTH};
    
    platforms[2] = (SDL_Rect){.x = 3*SCREEN_WIDTH/4, .y = SCREEN_HEIGHT/3,
                              .w = SCREEN_WIDTH/4, .h = VERTICAL_WIDTH};

    platforms[3] = (SDL_Rect){.x = 5*SCREEN_WIDTH/12, .y = SCREEN_HEIGHT/2,
                              .w = SCREEN_WIDTH/5, .h = VERTICAL_WIDTH};

    platforms[4] = (SDL_Rect){.x = 5*SCREEN_WIDTH/12, .y = 5*SCREEN_HEIGHT/6,
                              .w = SCREEN_WIDTH/4, .h = VERTICAL_WIDTH};
    
    return platforms;
}

struct Donut **Level_createDonuts(SDL_Renderer *renderer) {
    struct Donut **donuts = (struct Donut**) malloc(DONUTS_NUMBER * sizeof(struct Donut*));
    
    donuts[0] = Donut_create(renderer, SCREEN_WIDTH/6,  SCREEN_HEIGHT/4);
    donuts[1] = Donut_create(renderer, 5*SCREEN_WIDTH/6,  SCREEN_HEIGHT/4);
    donuts[2] = Donut_create(renderer, 3*SCREEN_WIDTH/4,  7*SCREEN_HEIGHT/12);

    return donuts;
}


void Level_deleteComponents(SDL_Rect *components) {
    if (components != NULL) {
        free(components);
    }
}

void Level_deleteDonuts(struct Donut **donuts) {
    if (donuts != NULL) {
        unsigned int i;
        for (i = 0; i < DONUTS_NUMBER; ++i)
            Donut_delete(donuts[i]);
        free(donuts);
    }
}

// ---------------- //
// Public functions //
// ---------------- //

struct Level *Level_create(SDL_Renderer *renderer) {
    struct Level *level = (struct Level*) malloc(sizeof(struct Level));
    level->renderer = renderer;    
    level->walls = Level_createWalls();
    level->donuts = Level_createDonuts(renderer);
    level->platforms = Level_createPlatforms();
    return level;
}

void Level_delete(struct Level *level) {
    if (level != NULL) {
        Level_deleteComponents(level->walls);
        Level_deleteComponents(level->platforms);
        Level_deleteDonuts(level->donuts);
        free(level);
    }
}

void Level_render(struct Level *level) {
    unsigned int i;
    for (i = 0; i < WALLS_NUMBER; ++i) {
        SDL_SetRenderDrawColor(level->renderer, 0xa1, 0xa9, 0xee, 0xFF);
        SDL_RenderFillRect(level->renderer, &(level->walls[i]));
    }
    unsigned int j;
    for (j = 0; j < PLATFORMS_NUMBER; ++j) {
        SDL_SetRenderDrawColor(level->renderer, 0xa1, 0xa9, 0xee, 0xFF);
        SDL_RenderFillRect(level->renderer, &(level->platforms[j]));
    }
    unsigned int k;
    for (k = 0; k < DONUTS_NUMBER; ++k) {
        Donut_render(level->donuts[k]);
    }    
}

bool Level_checkCollision(SDL_Rect *rect, struct Level *level) {
    bool collision = false;
    unsigned int i;
    for (i = 0; i < WALLS_NUMBER; ++i) {
        collision = collision || SDL_HasIntersection(rect, &(level->walls[i]));
    }
    unsigned int j;
    for (j = 0; j < PLATFORMS_NUMBER; ++j) {
        collision = collision || SDL_HasIntersection(rect, &(level->platforms[j]));
    }
    return collision;
}

void Level_updateDonuts(struct Level *level, SDL_Rect *rect) {
    unsigned int k;
    for (k = 0; k < DONUTS_NUMBER; ++k) {
        if (SDL_HasIntersection(rect, &(level->donuts[k]->boxCollider))) {
            level->donuts[k]->eaten = true;
        }
    }   
}

bool Level_hasNoDonuts(struct Level *level) {
    bool hasNoDonuts = true;
    unsigned int k;
    for (k = 0; k < DONUTS_NUMBER; ++k) {
        hasNoDonuts = hasNoDonuts && level->donuts[k]->eaten;
    }
    return hasNoDonuts;
}