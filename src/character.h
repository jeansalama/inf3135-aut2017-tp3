#ifndef CHARACTER_H
#define CHARACTER_H

#include "sdl2.h"
#include "animated_spritesheet.h"
#include <stdbool.h>

// --------------- //
// Data structures //
// --------------- //

struct Point { // Representing a 2D point
    int x;     // The x-coordinate
    int y;     // The y-coordinate
};

enum SpriteRow {
    CHARACTER_WALKING_RIGHT      = 0, // Character is walking to the right
    CHARACTER_WALKING_LEFT       = 1, // Character is walking to the left
    CHARACTER_IDLE               = 2, // Character is idle
    CHARACTER_JUMPING_VERTICALLY = 3, // Character is jumping vertically
    CHARACTER_JUMPING_RIGHT      = 4, // Character is jumping to the right
    CHARACTER_JUMPING_LEFT       = 5  // Character is jumping to the left
};

struct State {          // Representing state directions
    bool up;            // True if the up key is pressed
    bool left;          // True if the left key is pressed
    bool right;         // True if the right key is pressed
    bool touchGround;   // True if the character is touching the ground
};

struct Character {
    struct State state;                                // His current state
    enum SpriteRow spriteRow;                          // His current sprite row
    struct Point *position;                            // His position in the level
    struct Point *velocity;                            // His velocity
    struct AnimatedSpritesheet *animatedSpritesheet;   // His spritesheet
    SDL_Rect boxCollider;                              // His box collider
    SDL_Renderer *renderer;                            // The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates the character.
 *
 * @param renderer   The renderer
 * @return           A pointer to the character, NULL if there was an error;
 *                   Call IMG_GetError() for more information.
 */
struct Character *Character_create(SDL_Renderer *renderer);

/**
 * Deletes the character.
 *
 * @param character  The character to delete
 */
void Character_delete(struct Character *character);

/**
 * Renders the character.
 *
 * @param character  The character to render
 */
void Character_render(struct Character *character);

/**
 * Handles keyboard events for the character's velocity
 *
 * @param character  The character to render
 * @param renderer   The renderer
 */
void Character_handleEvent(struct Character *character, SDL_Event *event);

/**
 * Updates the sprite row of the character
 *
 * @param character  The character to render
 */
void Character_updateRow(struct Character *character);

#endif
