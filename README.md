# Travail pratique 3

## Description

Ce travail consiste en l'élaboration d'un jeu vidéo de plateformes à l'aide de
la bibliothèque [SDL2](https://www.libsdl.org/).

Ce travail est accompli dans le cadre du cours INF3135 "Construction et
maintenance de logiciels" (groupe 20, session d'automne 2017), enseigné par
Alexandre Blondin Massé, à l'UQAM.

## Auteur

Jean Salama (SALJ23048701)

## Fonctionnement

Le but de ce jeu est de déplacer un personnage (Bob) à l'aide des touches de
directions du clavier afin qu'il puisse manger (ou récupérer) tous les beignes
présents dans le niveau.

Tout d'abord, en lançant le jeu, vous verrez le menu avec deux options `1 -
Play` pour jouer et `2 - Quit` pour quitter le jeu (avec les touches `1` et `2`
du clavier):

![](images/menu.png)

Lors du déroulement du jeu, Bob (le personnage) doit se déplacer pour récupérer
tous les beignes présents dans le niveau, il peut :

- ne rien faire (rester immobile),
- marcher à droite,
- marcher à gauche,
- sauter verticallement,
- sauter à droite et
- sauter à gauche.

Ces déplacements s'effectuent à l'aide des touches de directions (flèches) du
clavier :

- `Haut` pour sauter
- `Droite` et `Gauche` pour les déplacements horizontaux

Par exemple, la combinaison des touches `Haut` et `Gauche` effectue le saut
vers la gauche.

![](images/deroulement.png)

#### Notes sur les déplacements

À noter que les déplacements de Bob ne suivent pas exactement les lois de la
physique.  En effet, si l'on appuye longtemps sur la touche `Haut` le
personnage continuera son saut (comme s'il pouvait voler). Cependant, dès qu'on
lache la touche `Haut`, Bob tombe sous l'effet de la gravité.

Une fois que tous les beignes ont été récupérés, un écran de fin s'affiche pour
annoncer la victoire avec l'option d'appuyer sur la touche `ESC` (Echap.) pour
retourner au menu.

![](images/fin.png)

## Plateformes supportées

Le jeu a été testé sur Linux Mint 18.2 Sonya.

## Dépendances

[SDL2](https://www.libsdl.org/), une bibliothèque permettant de concevoir des
applications graphiques.

## Compilation

Après avoir cloné le projet, lancez la compilation en ligne de commande depuis
la racine du projet avec la commande :
```sh
make
```
puis naviguez vers le répertoire `bin` et lancez le jeu `tp3` :
```sh
$ cd bin
$ ./tp3
```

Sinon, tout en demeurant dans le repertoire racine, la commande `make run`
lance la compilation puis exécute l'application. Ceci étant très utile en mode
développement, car il suffit de lancer la commande `make clean && make run`
après une modification du code.

## Références

- Ce jeu est une adaptation du [jeu
  Maze](https://bitbucket.org/ablondin-projects/maze-sdl) d'[Alexandre Blondin
  Massé](http://www.lacim.uqam.ca/~blondin/).
- Les tutoriels de [developpez.com](http://loka.developpez.com/tutoriel/sdl/)
  (SDL, en français) et de
  [lazyfoo.net](http://lazyfoo.net/tutorials/SDL/index.php) (SDL2), surtout les
  chapitres concernant les événements clavier, les mouvements et collisions.
- [3D Text Logo Creator](http://flamingtext.com/logo/Design-3D-Text) pour créer
  les images de textes pour le menu et l'écran de fin.

## Division des tâches

Note : j'ai effectué le travail seul, voici donc une liste résumant les tâches
effectuées.

- [X] Programmation du menu principal (Jean)
- [X] Conception des murs et des plateformes (Jean)
- [X] Ajout des donuts avec animation (Jean)
- [X] Gestion des mouvements et collisions du personnage (Jean)
- [X] Gestion des donuts "mangés" (Jean)
- [X] Gestion de l'écran de fin (Jean)
- [X] Gestion des feuilles de sprites et de l'animation du personnage (Jean)
- [X] Ajout de la documentation (Jean)

## Statut

Le projet a été complété et il est sans bogue.

Cependant il reste quelques détails à régler pour rendre les déplacements de
Bob plus réalistes :

- Les déplacements de Bob ne suivent pas exactement les lois de la physique
  (voir l'[explication](#notes-sur-les-déplacements) plus haut)
